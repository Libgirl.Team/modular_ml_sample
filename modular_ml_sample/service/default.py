from __future__ import division
import json
import numpy as np
import dong.framework
from modular_ml_sample.model.init.load_cnn_cifar10 import LoadCnnCifar10
from modular_ml_sample.data.cifar10 import Cifar10

class CnnCifar10Service(LoadCnnCifar10, dong.framework.Service):
    
    def serve(self, request_body, mime_type='application/json'):

        self.data = Cifar10().get_eval_data()
        
        n = json.loads(request_body)
        
        if 1 <= n <= 10000:
            prediction = np.argmax(self.predict(np.array([self.data.x[n - 1]]))[0]) + 1
            answer = np.argmax(self.data.y[n - 1]) + 1

            if prediction == answer:
                msg = 'Correctly predict sample # ' + str(n) + ' as category ' + str(answer) + '.'
            else:
                msg = 'Incorrectly predict sample # ' + str(n) + ' from category ' + str(answer) \
                + ' to ' + str(prediction) + '.'
                
            return json.dumps(msg)

        else:
            return json.dumps('pls enter a number between 1~10000 to specify the CIFAR10 test data.')


class DefaultService(CnnCifar10Service, dong.framework.Service):

    @dong.framework.request_handler
    def hello(self, request_body, mime_type='application/json'):
        return json.dumps('hello')

    @dong.framework.request_handler
    def serve(self, request_body, mime_type='application/json'):
        return super().serve(request_body, mime_type='application/json')
