from .cnn_cifar10 import CnnCifar10
from ...data.cifar10 import Cifar10

class LoadCnnCifar10(CnnCifar10):

    from ..serializer.default import read

    def __init__(self, config={}, data_params=None, save_dir=None):

        data = Cifar10()
        super().__init__(config,
                         data_params=data.get_data_params(),
                         save_dir=save_dir)
        self.read(save_dir)
