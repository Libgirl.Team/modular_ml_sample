import tensorflow
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D


class CnnCifar10(tensorflow.keras.models.Sequential):

    model_name = 'cnn_cifar10'
    
    def __init__(self, config={}, data_params=None, save_dir=None):

        super().__init__()
        self.add(Conv2D(32, (3, 3), padding='same',
                         input_shape=data_params.shape))
        self.add(Activation('relu'))
        self.add(Conv2D(32, (3, 3)))
        self.add(Activation('relu'))
        self.add(MaxPooling2D(pool_size=(2, 2)))
        self.add(Dropout(0.25))

        self.add(Conv2D(64, (3, 3), padding='same'))
        self.add(Activation('relu'))
        self.add(Conv2D(64, (3, 3)))
        self.add(Activation('relu'))
        self.add(MaxPooling2D(pool_size=(2, 2)))
        self.add(Dropout(0.25))

        self.add(Flatten())
        self.add(Dense(512))
        self.add(Activation('relu'))
        self.add(Dropout(0.5))
        self.add(Dense(data_params.num_labels))
        self.add(Activation('softmax'))
