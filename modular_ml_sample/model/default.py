from .init.cnn_cifar10 import CnnCifar10
import dong.framework

class DefaultModel(CnnCifar10, dong.framework.Model):

    from .train.rmsp_ctgr_x_entropy import train
    from .serializer.default import write
