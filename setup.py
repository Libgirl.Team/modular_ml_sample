from setuptools import setup, find_packages

setup(name='modular_ml_sample',
      version='0.1',
      description='none',
      license='MIT',
      install_requires=[
          'tensorflow',
      ],
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      entry_points = {
        'console_scripts': ['modular_ml_sample=modular_ml_sample:main'],
      },
    )
